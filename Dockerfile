FROM tomcat:8.0
MAINTAINER andyqac
COPY target/*.war /usr/local/tomcat/webapps/hello-scalatra.war
EXPOSE 8080
CMD ["cataline.sh", "run"]
